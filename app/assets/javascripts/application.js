// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


var ready = function() {
    $('body').on('click','#add_form_question',function () {
        var total_questions = $('.question_container').length;
        addQuestion(total_questions);
        return false;
    }).on('click','.rem_choice',function(){
        var my_this = this;
        var my_id = $(my_this).attr('data-id');
        $('#question_container_'+my_id).remove();
        return false;
    }).on('change','.form_questions_type',function(){
        var my_this = this;
        var my_id = $(my_this).attr('data-id');
        var select_val = $(my_this).val();
        if(select_val=='text'){
            $('#choices_container_'+my_id).hide().html('');
        }else{
            var total_question_choices = $('#choices_container_'+my_id+' .choices').length;
            if(total_question_choices==0) {
                addChoice(my_id, total_question_choices);
                $('#choices_container_' + my_id).show();
            }
        }
    }).on('click','.add_ques_choice',function(){
        var my_this = this;
        var parent_id = $(my_this).attr('data-parent');
        var total_question_choices = $('#choices_container_'+parent_id+' .choices').length;
        addChoice(parent_id,total_question_choices);
        return false;
    }).on('click','.rem_ques_choice',function(){
        var my_this = this;
        var parent_id = $(my_this).attr('data-parent');
        var data_id = $(my_this).attr('data-id');
        $('#question_choice_container_'+parent_id+'_'+data_id).remove();
        return false;
    });
};

function addQuestion(total_questions,value){
    // question container
    var question_dv = document.createElement('div');
    $(question_dv).attr('id','question_container_'+total_questions);
    $(question_dv).addClass('row question_container');

    var question_input_dv = document.createElement('div');
    $(question_input_dv).addClass('col-sm-8');
    $(question_dv).html(question_input_dv);
    var question_input_form_group = document.createElement('div');
    $(question_input_form_group).addClass('form-group');
    $(question_input_dv).html(question_input_form_group);

    var question_type_dv = document.createElement('div');
    $(question_type_dv).addClass('col-sm-2');
    $(question_dv).append(question_type_dv);
    var question_type_form_group = document.createElement('div');
    $(question_type_form_group).addClass('form-group');
    $(question_type_dv).html(question_type_form_group);

    // question name input
    var question_input = document.createElement('input');
    $(question_input).attr('id','form_questions_name_'+total_questions);
    $(question_input).attr('name','form[question][][name]');
    $(question_input).attr('data-id',total_questions);
    $(question_input).addClass('form_questions_name form-control');
    $(question_input).attr('placeholder','Question');
    if(value!=undefined){
        $(question_input).val(value);
    }
    $(question_input_form_group).html(question_input);

    // question type select
    var question_type = document.createElement('select');
    var question_types = {
        'text': 'Text',
        'dropdown': 'DropDown',
        'checkboxes': 'CheckBoxes (Multi Select)',
        'radiobuttons': 'Radio Buttons (Only One Select)'
    };
    for(var val in question_types) {
        var question_type_option = document.createElement('option');
        $(question_type_option).val(val);
        $(question_type_option).html(question_types[val]);
        $(question_type).append(question_type_option);
    }
    $(question_type).attr('id','form_questions_type_'+total_questions);
    $(question_type).attr('name','form[question][][question_type]');
    $(question_type).attr('data-id',total_questions);
    $(question_type).addClass('form_questions_type form-control');
    $(question_type).attr('placeholder','Type');
    $(question_type_form_group).html(question_type);



    var question_options_dv = document.createElement('div');
    $(question_options_dv).addClass('col-sm-2');
    $(question_options_dv).addClass('add_rem');

    if(total_questions>0){
        $(question_options_dv).addClass('add_rem');
        var rem_href = document.createElement('a');
        $(rem_href).attr('data-id',total_questions);
        $(rem_href).attr('href','#');
        $(rem_href).addClass('rem_choice');
        $(rem_href).html('<span class="glyphicon glyphicon-minus-sign"></span>');
        $(question_options_dv).append(rem_href);
    }
    $(question_dv).append(question_options_dv);

    var choices_dv = document.createElement('div');
    $(choices_dv).attr('id','choices_container_'+total_questions);
    $(choices_dv).addClass('col-sm-12 question_container_'+total_questions);
    $(question_dv).append(choices_dv);


    $('#form-questions').append(question_dv);

}

function addChoice(my_id,total_question_choices, value){
    var div_container = document.createElement('div');
    $(div_container).attr('id','question_choice_container_'+my_id+'_'+total_question_choices);

    var choice_dv = document.createElement('div');
    $(choice_dv).addClass('col-sm-10 choices choices_'+my_id+'_'+total_question_choices);
    $(choice_dv).attr('id','choice_'+my_id+'_'+total_question_choices);
    $(div_container).append(choice_dv);

    var choice_options_dv = document.createElement('div');
    $(choice_options_dv).addClass('col-sm-2');
    $('#choices_container_'+my_id).append(div_container);
    $(div_container).append(choice_options_dv);

    var add_href = document.createElement('a');
    $(add_href).attr('data-parent',my_id);
    $(add_href).attr('data-id',total_question_choices);
    $(add_href).attr('href','#');
    $(add_href).addClass('add_ques_choice');
    $(add_href).html('<span class="glyphicon glyphicon-plus-sign"></span>');
    $(choice_options_dv).append(add_href);

    if(total_question_choices>0) {
        var rem_href = document.createElement('a');
        $(rem_href).attr('data-parent', my_id);
        $(rem_href).attr('data-id', total_question_choices);
        $(rem_href).attr('href', '#');
        $(rem_href).addClass('rem_ques_choice');
        $(rem_href).html('<span class="glyphicon glyphicon-minus-sign"></span>');
        $(choice_options_dv).append(rem_href);
    }

    var choice_form_group = document.createElement('div');
    $(choice_form_group).addClass('form-group');
    $(choice_dv).html(choice_form_group);

    // question name input
    var choice_input = document.createElement('input');
    $(choice_input).attr('id','form_questions_'+my_id+'_choice_'+total_question_choices);
    $(choice_input).attr('name','form[question][][choice][]');
    $(choice_input).attr('data-id',total_question_choices);
    $(choice_input).attr('data-parent',my_id);
    if(value!=undefined) {
        $(choice_input).val(value);
    }
    $(choice_input).addClass('form_questions_choice form-control');
    $(choice_input).attr('placeholder','Choice');
    $(choice_form_group).html(choice_input);

}
$(document).on('turbolinks:load', ready);
