json.array!(@users) do |user|
  json.extract! user, :id, :username, :password, :first_name, :last_name, :gender
  json.url users_url(user, format: :json)
end