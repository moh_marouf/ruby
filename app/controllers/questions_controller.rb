class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :edit, :update, :destroy]

  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.all
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
  end

  # GET /questions/new
  def new
    @question = Question.new
  end

  # GET /questions/new/:id
  def new_ques
    @question = Question.new
    @form = Form.find(params[:id])
    @question.form = @form
    render '_form_new'
  end

  # GET /questions/1/edit
  def edit
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params)
    @question.choices = Array.new()

    respond_to do |format|
      if @question.question_type!="text"
        params[:form][:question].each_with_index do |ques|
          ques[:choice].each_with_index do |choice|
            if choice!=''
              my_choice = Choice.new
              my_choice.name = choice
              @question.choices.push(my_choice)
            end
          end
        end
      end
      if @question.choices.length
        if @question.save
          @question.choices.each do |choice|
            choice.question_id = @question.id
            choice.save
          end
          format.html { redirect_to @question.form, notice: 'Question was successfully created.' }
          format.json { render :show, status: :created, location: @question }
        else
          format.html { render :new }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to @question.form, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    my_form = @question.form
    if @question.question_type != 'text'
      @question.choices.each do |choice|
        choice.destroy
      end
    end
    @question.destroy
    respond_to do |format|
      format.html { redirect_to my_form, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.fetch(:question).permit(:name, :question_type, :form_id)
    end
end
