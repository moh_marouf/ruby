class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :require_login

  include SessionsHelper

  private
  def require_login
    unless current_user
      redirect_to login_url
    end
  end

  private
  def require_logout
    unless current_user==nil
      redirect_to users_url
    end
  end
end
