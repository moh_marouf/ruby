class FormsController < ApplicationController
  before_action :set_form, only: [:show, :edit, :update, :destroy, :results, :showFormQuestions, :submitFormQuestions]
  skip_before_filter :require_login, :only=>[:showFormQuestions,:submitFormQuestions]

  # GET /forms
  # GET /forms.json
  def index
    @forms = Form.where({:user_id => current_user().id})
    if @forms
      render :index
    else
      render :error
    end
  end

  # GET /forms/1
  # GET /forms/1.json
  def show
    if @form != false
      render :show
    else
      render :error
    end
  end

  # GET /forms/new
  def new
    @form = Form.new
  end

  # GET /forms/1/edit
  def edit
    if @form != false
      render :edit
    else
      render :error
    end
  end

  # POST /forms
  # POST /forms.json
  def create
    @form = Form.new(form_params)
    @form.user_id = current_user().id

    respond_to do |format|
      valid = true
      questions = Array.new
      if params[:form].has_key?(:question)
        params[:form][:question].each_with_index do |question|
          my_question = Question.new
          if question[:name]==''
            @form.errors.add(:base,'Some Questions Are Empty')
            valid = false
            break
          else
            my_question.name = question[:name]
            my_question.question_type = question[:question_type]
          end
          if question[:question_type]!='text'
            if question.has_key?(:choice)
              question[:choice].each_with_index do |choice|
                if choice!=''
                  my_choice = Choice.new
                  my_choice.name = choice
                  my_question.choices.push(my_choice)
                else
                  @form.errors.add(:base,'Some Choices Are Empty')
                  valid = false
                  break
                end
              end
            else
              @form.errors.add(:base,'None Text Questions Not Have Choices')
              valid = false
              break
            end
          end
          questions.push(my_question)
        end
      else
        @form.errors.add(:base,'Undefined Questions')
        valid = false
      end
      if valid && @form.save
        questions.each do |question|
          question.form_id = @form.id
          if question.save
            question.choices.each do |choice|
              choice.question_id = question.id
              choice.save
            end
          end
        end
        format.html { redirect_to @form, notice: 'Form was successfully created.' }
        format.json { render :show, status: :created, location: @form }
      else
        format.html { render :new }
        format.json { render json: @form.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forms/1
  # PATCH/PUT /forms/1.json
  def update
    if @form != false
      respond_to do |format|
        if @form.update(form_params)
          format.html { redirect_to @form, notice: 'Form was successfully updated.' }
          format.json { render :show, status: :ok, location: @form }
        else
          format.html { render :edit }
          format.json { render json: @form.errors, status: :unprocessable_entity }
        end
      end
    else
      render :error
    end
  end

  # DELETE /forms/1
  # DELETE /forms/1.json
  def destroy
    if @form != false
      @form.questions.each do |question|
        if question.question_type != 'text'
          question.choices.each do |choice|
            choice.destroy
          end
        end
        question.destroy
      end
      @form.destroy
      respond_to do |format|
        format.html { redirect_to forms_url, notice: 'Form was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      render :error
    end
  end

  # ShowForm /forms/1/
  def showFormQuestions
    if current_user().id == @form.user_id
      @form_answer = FormAnswer.new
      @form_answer.form = @form
      render :show_form
    else
      respond_to do |format|
        format.html { redirect_to forms_url, notice: 'You cannot answer your form.' }
        format.json { head :no_content }
      end
    end
  end

  # submit_answer
  def submitFormQuestions
    if current_user().id == @form.user_id
      @form.questions.each do |question|
        @answers = params["ques_#{question.id}"]
        if !@answers.kind_of?(Array)
          @answers = [@answers]
        end
        @answers.each do |answer|
          form_answer = FormAnswer.new
          form_answer.form_id = @form.id
          form_answer.question_id = question.id
          form_answer.answer = answer
          if question.question_type != 'text'
            choice = Choice.find(answer)
            form_answer.choice_id = choice.id
            form_answer.answer = choice.name
          end
          form_answer.save
        end
      end
      respond_to do |format|
        format.html { redirect_to forms_url, notice: 'Form was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to forms_url, notice: 'You cannot answer your form.' }
        format.json { head :no_content }
      end
    end
  end

  # GET results/1
  # POST results/1
  # POST results/1.pdf
  def results
    @format = 'html'
    where_statement = {}
    where_statement[:form_id] = @form.id
    joins = {:question=>{}}
    if params[:question] && params[:question]!=''
      where_statement[:question_id] = params[:question]
    end
    if params[:question_type] && params[:question_type]!=''
      where_statement[:questions] = {:question_type=>params[:question_type]}
    end
    @answers = FormAnswer.joins(:question).where(where_statement)
    respond_to do |format|
      format.html
      format.pdf do
        @format = 'pdf'
        render :pdf => 'report', :layout=>'pdf.html.haml'
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form
      @form = Form.where({:id=>params[:id],:user_id=>current_user().id})
      if @form.length>0
        @form = @form[0]
      else
        @form = false
      end
    end
    def set_public_form
      @form = Form.where({:id=>params[:id],:user_id=>current_user().id})
      if @form.length>0
        @form = @form[0]
      else
        @form = false
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def form_params
      params.fetch(:form).permit(:name, :desc)
    end
end
