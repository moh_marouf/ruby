class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "is not an email")
    end
  end
end

class User < ActiveRecord::Base

  validates :username, uniqueness: true
  validates :email, presence: true, email: true
  has_secure_password
  # def authenticate(pass)
  #   if pass=self.pass
  #     return true
  #   else
  #     return false
  #   end
  # end
end
