class Form < ActiveRecord::Base
  has_many :questions
  has_many :choices, through: :questions
  has_many :form_answers
end
