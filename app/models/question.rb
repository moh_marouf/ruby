class Question < ActiveRecord::Base
  belongs_to :form
  has_many :choices
  has_many :form_answers
end
