class FormAnswer < ActiveRecord::Base
  belongs_to :form
  belongs_to :question
  belongs_to :choice
end
