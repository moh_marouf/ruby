class ChangeUserPasswordFieldName < ActiveRecord::Migration
  def change
    rename_column :users, :password, :pass
  end
end
