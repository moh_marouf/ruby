class ChangeTypeColumnTable < ActiveRecord::Migration
  def change
    remove_column :choices, :type, :string
    add_column :questions, :type, :string
  end
end
