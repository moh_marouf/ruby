class CreateFormAnswers < ActiveRecord::Migration
  def change
    create_table :form_answers do |t|
      t.belongs_to :form, index: true, foreign_key: true
      t.belongs_to :question, index: true, foreign_key: true
      t.belongs_to :choice, index: true, foreign_key: true
      t.string :answer
    end
  end
end
