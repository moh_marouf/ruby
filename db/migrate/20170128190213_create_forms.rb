class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.string :name
      t.text :desc
      t.belongs_to :user, index=true
      t.timestamps null: false
    end
  end
end

