# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170203220916) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "choices", force: :cascade do |t|
    t.string   "name"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "choices", ["question_id"], name: "index_choices_on_question_id", using: :btree

  create_table "form_answers", force: :cascade do |t|
    t.integer "form_id"
    t.integer "question_id"
    t.integer "choice_id"
    t.string  "answer"
  end

  add_index "form_answers", ["choice_id"], name: "index_form_answers_on_choice_id", using: :btree
  add_index "form_answers", ["form_id"], name: "index_form_answers_on_form_id", using: :btree
  add_index "form_answers", ["question_id"], name: "index_form_answers_on_question_id", using: :btree

  create_table "forms", force: :cascade do |t|
    t.string   "name"
    t.text     "desc"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.text     "name"
    t.integer  "form_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "question_type"
  end

  add_index "questions", ["form_id"], name: "index_questions_on_form_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "gender"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "email"
    t.string   "password_digest"
  end

  create_table "widgets", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "stock"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_foreign_key "choices", "questions"
  add_foreign_key "form_answers", "choices"
  add_foreign_key "form_answers", "forms"
  add_foreign_key "form_answers", "questions"
  add_foreign_key "questions", "forms"
end
